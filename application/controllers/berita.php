<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

	public function index()
	{
		$data['main_view'] = 'berita_view';
		$this->load->view('template', $data);
	}

}

/* End of file berita.php */
/* Location: ./application/controllers/berita.php */