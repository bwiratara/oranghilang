<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail_berita extends CI_Controller {

	public function index()
	{
		$data['main_view'] = 'detail_berita_view';
		$this->load->view('template', $data);		
	}

}

/* End of file detail_berita.php */
/* Location: ./application/controllers/detail_berita.php */