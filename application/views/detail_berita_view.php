	<section style="margin-top: 150px; ">
		<div class="container">
			<div class="col-12 col-md-8" style=" border-right:  3px dashed #ddd;">
				<h1 class="post-title">Bencana Palu kembali lagi!</h1>
				<div >by <a href="">Humas</a></div>
				<p align="center"><img src="<?=base_url()?>assets/images/img-bg-01.png" style="width: 377px; height: 251px; margin-top: 20px; margin-bottom:20px;"></p>
				 <div><i class="fa fa-calendar fa-fw"></i> 27 Desember 2018
                 <div >
					<i class="fa fa-tag"></i> 
					<a href="/berita/tag/berita">Berita</a>, 
					<a href="/berita/tag/20-daerah-istimewa-yogyakarta">Daerah Istimewa Yogyakarta</a>, 
					<a href="/berita/tag/21-jogja">Jogja</a>, <a href="/berita/tag/22-pemda-diy">Pemda DIY</a>                
				</div></div>

				<p><p><strong>Yogyakarta (27/12/2018) stikibencana.com </strong>&ndash;
				Masyarakat dilarang menerbangkan <em>drone</em> di jalur penerbangan yakni di kawasan udara di atas rel dari Stasiun Tugu ke timur.&nbsp; Pengoperasian pesawat nir awak yang dikendalikan dari jarak jauh di jalur <em>landing</em> akan sangat membahayakan keselamatan penerbangan.</p>
				
				<p>Demikian dikatakan Kepala Dinas Operasi Lanud Adisutjipto Yogyakarta Kolonel (Pnb) Andi Wijanarko pada&nbsp;Rapat Koordinasi/Monev Pemantapan Menjelang Tahun Baru 2019 di Hotel Santika, Yogyakarta, Kamis (27/12).&nbsp; Rapat dipimpin Sekretaris Daerah (Sekda) DIY Ir Gatot Saptadi mewakili Gubernur DIY Sri Sultan Hamengku Buwono X dengan dihadiri Polda DIY, Kesbangpol DIY dan instansi terkait lain.</p>
				
				<p>''Tahun lalu ada yang menerbangkan <em>drone</em> di garis finalnya pesawat yakni di atas rel sekitar Stasiun Tugu dan Lempuyangan.&nbsp; Ini sangat berbahaya. Maka tahun ini diharapkan jangan ada lagi. Dilarang menerbangkan <em>drone</em> tanpa seizin otoritas penerbangan di jalur itu.&nbsp; Dulu ada 3 yang diterbangkan tepat pada malam pergantian tahun sehingga 3 pesawat kami <em>delay</em> sampai <em>drone</em> ditertibkan.&nbsp; Ini untuk keselamatan bers
				
				</p>

				<p align="center"><img src="<?=base_url()?>assets/images/img-bg-01.png" alt="" width="377" height="251" /></p>
				<p>''Intinya kita siap menyambut pergantian tahun. Selain sektor lalu lintas, stok komoditi dipantau agar kebutuhan pangan tercukupi.&nbsp;Distribusi (komoditi pangan) juga (dipantau). Potensi bencana alam yang mungkin terjadi tidak boleh luput dari perhatian. Melalui pantauan di sekitar pantai dan pemantauan Gunung Merapi,'' kata Sekda.</p>
				
				<p>Ditambahkan, salah satu yang menjadi perhatian utama adalah penanganan lalu lintas.&nbsp; Untuk itu pada rentang 21 Desember 2018 sampai 2 Januari 2019 Polda DIY menggelar Operasi Lilin Progo 2018.&nbsp; Untuk mengantisipasi kemacetan, ada beberapa titik pemindahan arus dan pembukaan jalur alternatif.</p>
				
				<p>&ldquo;Juga sudah memasang rambu-rambu petunjuk &nbsp;arah dan menambah lampu APILL di lokasi yang <em>crowded</em>, serta menyediakan pos pelayanan dan pos pengamanan,&rdquo; terangnya.&nbsp;</p>

				<p>Pengamanan yang dilakukan meliputi di jalan raya, tempat ibadah, pusat keramaian, serta kawasan wisata.&nbsp; Pelaksanaan pengamanan dilakukan kolaboratif antara Polda DIY dengan TNI dan Satpol PP DIY serta unsur-unsur lain.</p>

				<p>Terkait kesehatan, Dinas Kesehatan DIY telah menyiapkan layanan <em>Call</em> 119 yang terhubung di setiap kabupaten-kota untuk kebutuhan <em>emergency.</em> Pos-pos kesehatan disediakan di setiap wilayah. Persediaan stok obat akan terus dipantau.&nbsp; Pelayanan medis dilakukan bekerja sama dengan PMI DIY.</p>

				<p>''Harapannya menjelang Tahun Baru 2019 dan sesudahnya DIY tetap aman terkendali dan tetaplah menjadi tuan rumah yang baik dan membawa nama baik keistimewaan Jogja,'' kata Sekda DIY.&nbsp; <strong>(*/naz)</strong></p>
				<p>HUMAS DIY</p>

			</p>
			</div>

			<div class="col-md-4" style="">
					
	                <div class="sidebar-widget-area" style="margin-bottom: 30px">
	                    <h5 class="title" style="">Tag</h5>
	                    <div class="widget-content">
	                    	<span style="background: yellow; padding:5px; margin-left: 10px; position:relative;  ">
	                    		<a href="" >
	                    		Bencana
	                    		</a>
	                    	</span>
	                    	<span style="background: yellow; padding:5px; margin-left:10px;position: relative;  ">
	                    		<a href="" >
	                    		Bencana
	                    		</a>
	                    	</span>
	                    	<span style="background: yellow; padding:5px; margin-left:10px;position: relative;  ">
	                    		<a href="" >
	                    		Bencana
	                    		</a>
	                    	</span>
	                    </div>
	                </div>

	                <div class="sidebar-widget-area">
	                    <h5 class="title">Most Read</h5>
	                    <div class="widget-content">
	                        <!-- Single Blog Post -->
	                        <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
	                            <!-- Post Thumbnail -->
	                            <div class="post-thumbnail">
	                                <img src="<?=base_url()?>assets/images/b11.jpg" alt="">
	                            </div>
	                            <!-- Post Content -->
	                            <div class="post-content" style="margin-left:10px;">
	                                <a href="#" class="headline" >
	                                    <h5 class="mb-0">How Did van Gogh’s Turbulent Mind Depict One of the Most</h5>
	                                </a>
	                            </div>
	                        </div>
	                        <!-- Single Blog Post -->
	                        
	                    </div>
	                </div>


			</div>		
		</div>
	</section>