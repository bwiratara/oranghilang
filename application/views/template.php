<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Stiki Bencana</title>
	
	<link rel="icon" type="image/png" href="<?=base_url()?>assets/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/fonts/font-awesome-4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/all.css">
	
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700|Source+Sans+Pro:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
</head>
<body>
	
<div id="wrapper">
		<header class="header">
			<div class="container">
				<div class="logo"><a href="<?=base_url()?>assets/#"><img src="<?=base_url()?>assets/images/logo.png" alt="Sports"></a></div>
				<nav id="nav">
					<div class="opener-holder">
						<a href="<?=base_url()?>assets/#" class="nav-opener"><span></span></a>
					</div>
					<a href="<?=base_url()?>assets/javascript:" class="btn btn-primary rounded">Login</a>
					<div class="nav-drop">
						<ul>
							<li class="active visible-sm visible-xs"><a href="<?=base_url()?>assets/#">Home</a></li>
							<li><a href="<?php echo base_url('index.php/home'); ?>" >Beranda</a></li>
							<li><a href="<?=base_url()?>assets/#" >Data Profil</a></li>
							<li><a href="<?php echo base_url('index.php/berita'); ?>">Berita</a></li>
						</ul>
						<div class="drop-holder visible-sm visible-xs">
							<span>Follow Us</span>
							<ul class="social-networks">
								<li><a class="fa fa-github" href="<?=base_url()?>assets/#"></a></li>
								<li><a class="fa fa-twitter" href="<?=base_url()?>assets/#"></a></li>
								<li><a class="fa fa-facebook" href="<?=base_url()?>assets/#"></a></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
		</header>
	
	<div >
		 <?php
                $this->load->view($main_view);
            ?>
	</div>
	<br>
	<section style="background-color:rgb(216, 88, 37);" class="footer"> 
	<h1 class="text-center" style="padding-top:1%;margin:0px;color: white;font-size:2vw;font-weight:bold">Lembaga Terkait</p>
		<div class="container-fluid" style="padding-bottom:1%">
		<div class="col-xs-6">
		
				
					<img src="<?=base_url()?>assets/images/fot1.png" style="width:auto;height:auto" class="rounded float-right" alt="">
		
		
		</div>
		<div class="col-xs-6">
				
						<img src="<?=base_url()?>assets/images/fot2.png" style="width:auto;height:auto" class="rounded float-left" alt="">
				
		</div>
	
	</div>
	<p class="text-center" style="padding-top:1%;padding-bottom:1%;margin: 0;background-color:rgb(167, 66, 26);;color:white;font-size: 1vw;font-weight: 100;">&copy; 2018 DimanaKeluargaku</p>
</section>

<script src="<?=base_url()?>assets/js/jquery-1.11.2.min.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap.js"></script>
<script src="<?=base_url()?>assets/js/jquery.main.js"></script>
</body>
</html>
<script type="text/javascript">
	$(window).scroll(function() {    

    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $(".header").addClass("darkHeader");
    } else {
        $(".header").removeClass("darkHeader")    
    }
});
</script>