		<section>
		  <div class="container">

			<div class="col-md-8 " style="margin-top: 90px;  border-right:  3px dashed #ddd; ">
				<div class="world-latest-articles">
	                <div class="row">
	                    <div class="col-12 col-lg-11">
	                        <div class="title">
	                            <h5>Latest Articles</h5>
	                        </div>

	                        <!-- Single Blog Post -->
	                        <div class="single-blog-post post-style-4 d-flex align-items-center wow fadeInUpBig" data-wow-delay="0.2s">
	                            <!-- Post Thumbnail -->
	                            <div class="post-thumbnail">
	                                <img src="<?=base_url()?>/assets/images/b18.jpg" alt="">
	                            </div>
	                            <!-- Post Content -->
	                            <div class="post-content">
	                                <a href="<?php echo base_url('index.php/detail_berita'); ?>" class="headline">
	                                    <h5>Bencana Palu</h5>
	                                </a>
	                                <p>Sejak gempa dan tsunami melanda Palu dan daerah sekitarnya di Sulawesi Tengah pada 28 September lalu, lebih ...</p>
	                                <!-- Post Meta -->
	                                <div class="post-meta">
	                                    <p><a href="#" class="post-author">Steven</a> on <a href="#" class="post-date">Des 29, 2018 at 9:48 am</a></p>
	                                </div>
	                            </div>
	                        </div>


	                        <!-- Single Blog Post -->
	                        <div class="single-blog-post post-style-4 d-flex align-items-center wow fadeInUpBig" data-wow-delay="0.3s">
	                            <!-- Post Thumbnail -->
	                            <div class="post-thumbnail">
	                                <img src="<?=base_url()?>/assets/images/b18.jpg" alt="">
	                            </div>
	                            <!-- Post Content -->
	                            <div class="post-content">
	                                <a href="<?php echo base_url('index.php/detail_berita'); ?>" class="headline">
	                                    <h5>Bencana Palu</h5>
	                                </a>
	                                <p>Sejak gempa dan tsunami melanda Palu dan daerah sekitarnya di Sulawesi Tengah pada 28 September lalu, lebih ...</p>
	                                <!-- Post Meta -->
	                                <div class="post-meta">
	                                    <p><a href="#" class="post-author">Steven</a> on <a href="#" class="post-date">Des 29, 2018 at 9:48 am</a></p>
	                                </div>
	                            </div>
	                        </div>

	                        <!-- Single Blog Post -->
	                        <div class="single-blog-post post-style-4 d-flex align-items-center wow fadeInUpBig" data-wow-delay="0.4s">
	                            <!-- Post Thumbnail -->
	                            <div class="post-thumbnail">
	                                <img src="<?=base_url()?>/assets/images/b18.jpg" alt="">
	                            </div>
	                            <!-- Post Content -->
	                            <div class="post-content">
	                                <a href="#" class="headline">
	                                    <h5>Bencana Palu</h5>
	                                </a>
	                                <p>Sejak gempa dan tsunami melanda Palu dan daerah sekitarnya di Sulawesi Tengah pada 28 September lalu, lebih ...</p>
	                                <!-- Post Meta -->
	                                <div class="post-meta">
	                                    <p><a href="#" class="post-author">Steven</a> on <a href="#" class="post-date">Des 29, 2018 at 9:48 am</a></p>
	                                </div>
	                            </div>
	                        </div>

	                        <!-- Single Blog Post -->
	                        <div class="single-blog-post post-style-4 d-flex align-items-center wow fadeInUpBig" data-wow-delay="0.5s">
	                            <!-- Post Thumbnail -->
	                            <div class="post-thumbnail">
	                                <img src="<?=base_url()?>/assets/images/b18.jpg" alt="">
	                            </div>
	                            <!-- Post Content -->
	                            <div class="post-content">
	                                <a href="#" class="headline">
	                                    <h5>Bencana Palu</h5>
	                                </a>
	                                <p>Sejak gempa dan tsunami melanda Palu dan daerah sekitarnya di Sulawesi Tengah pada 28 September lalu, lebih ...</p>
	                                <!-- Post Meta -->
	                                <div class="post-meta">
	                                    <p><a href="#" class="post-author">Steven</a> on <a href="#" class="post-date">Des 29, 2018 at 9:48 am</a></p>
	                                </div>
	                            </div>
	                        </div>
	                    </div>

	                
	                </div>
            	</div>
			</div>
				
			<div class="col-md-3 " style="margin-top: 90px; margin-left: 50px; " >
				<div class="world-latest-articles">
					<div class="row">
						<div class="title">
							<h5>Twitter BMKG</h5>
						</div>
					<div class="single-blog-post post-style-4 d-flex align-items-center wow fadeInUpBig" data-wow-delay="0.2s">
						<a class="twitter-timeline" class="twitter" data-width="300" data-height="700" data-theme="dark" href="https://twitter.com/infoBMKG?ref_src=twsrc%5Etfw">Tweets by infoBMKG</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
					</div>
					</div>
				</div>
				
			</div>
		  </div>	
		</section>