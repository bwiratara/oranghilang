<section class="visual">
		<div class="container">
			<div class="text-block">
				<div class="heading-holder">
					<h1>Design your sports</h1>
				</div>
				<p class="tagline">A happier & healthier life is waiting for you!</p>
				<span class="info">Get motivated now</span>
			</div>
		</div>
		<img src="<?=base_url()?>assets/images/img-bg-01.png" alt="" class="bg-stretch">
	</section>
	<section class="main" style="background-color: #C4C653;">
		<div class="pertengah">
		<h1 style="color:white;margin-left: 15%;font-size: 3vw;font-weight: bold;margin-top: -2%;margin-bottom: 3%;;">Temukan Keluarga Anda ! </h1>
		<select name="carlist" form="carform" t style="
	background: rgb(212, 45, 45);
	color: white;
		font-family: arial;
		font-size: 2vw;
		width: auto;
		height: auto;
		border-radius: 2vw;
		padding-top: 1vw;
		padding-right: 1vw;
		padding-left: 1vw;
		padding-bottom: 1vw;
		margin-left: 8%;
		margin-right: 1%;

	
	">
				<option value="Anyer">Anyer</option>
				<option value="Pandeglang">Pandeglang</option>
				<option value="Lampung">Lampung</option>
			  </select>
			<input class="input" type="text" name="" placeholder="Cari disini..." class="find2" style="height:auto;font-size: 2vw; margin-left:9%;width: auto;">
          	<a href="#" ><img src="<?= base_url()?>assets/images/find.png" class="find" style="width:2vw;height:auto"></a> 
		</div>
	</section>
	
	<div class="container">
			<div class="row">
			  <div class="col-md-8 ">
					<h1 style="font-weight: bold;margin-left: 33%;margin-top:3%;margin-bottom: 2%;font-size: 3vw;">BERITA TERBARU</h1>
					<div id="myCarousel" class="carousel slide" data-ride="carousel" style="width:auto;height:auto">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>		
						</ol>
			 
						<!-- deklarasi carousel -->
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img style="width:100%;height:auto" src="<?=base_url()?>assets/images/tes.png" alt="">
								<div class="carousel-caption">
									<h1>Aerial Foto</h1>
									<h5>Sisa Rumah Warga Setelah Diterjang Tsunami</h5>
								</div>
							</div>
							<div class="item">
									<img style="width:100%;height:auto" src="<?=base_url()?>assets/images/tes.png" alt="">
								<div class="carousel-caption">
									<h1>Aerial Foto</h1>
									<h5>Sisa Rumah Warga Setelah Diterjang Tsunami</h5>
								</div>
							</div>
							<div class="item">
									<img style="width:100%;height:auto" src="<?=base_url()?>assets/images/tes.png" alt="">
								<div class="carousel-caption">
									<h1>Aerial Foto</h1>
									<h5>Sisa Rumah Warga Setelah Diterjang Tsunami</h5>
								</div>
							</div>				
						</div>
			 
						<!-- membuat panah next dan previous -->
						<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
					
					<div class="row">
							<div class="col-sm-12"  style="margin-top:5%">
								  <div class="col-xs-6">
										  <!-- Single Blog Post -->
										  <div class="single-blog-post wow fadeInUpBig" data-wow-delay="0.2s">
											  <!-- Post Thumbnail -->
											  <div class="post-thumbnail">
												  <img src="<?=base_url()?>assets/images/b2.jpg" alt="" style="width:100%;height:auto">
												  <div class="post-meta">
														<p><a href="#" class="post-author">Katy Liu</a> on <a href="#" class="post-date">Sep 29, 2017 at 9:48 am</a></p>
													</div>
											  </div>
											  <!-- Post Content -->
											  <div class="post-content">
												  <a href="#" class="headline">
													  <h5 style="font-weight:bold;font-color: orange;text-decoration: none;color: orange;font-size: 1.3vw;">Posko di Pelabuhan Anyer Butuh Bantuan</h5>
												  </a
												  <!-- Post Meta -->
												  
											  </div>
										  </div>
									  </div>
									  <div class="col-xs-6">
											<!-- Single Blog Post -->
											<div class="single-blog-post wow fadeInUpBig" data-wow-delay="0.2s">
												<!-- Post Thumbnail -->
												<div class="post-thumbnail">
													<img src="<?=base_url()?>assets/images/b2.jpg" alt="" style="width:100%;height:auto">
													<div class="post-meta">
															<p><a href="#" class="post-author">Katy Liu</a> on <a href="#" class="post-date">Sep 29, 2017 at 9:48 am</a></p>
												</div>
												</div>
												<!-- Post Content -->
												<div class="post-content">
													<a href="#" class="headline">
														<h5 style="font-weight:bold;font-color: orange;text-decoration: none;color: orange;font-size: 1.3vw;">Posko di Pelabuhan Anyer Butuh Bantuan</h5>
													</a>
													<!-- Post Meta -->
													
												</div>
											</div>
										</div>
										<div class="col-xs-6">
												<!-- Single Blog Post -->
												<div class="single-blog-post wow fadeInUpBig" data-wow-delay="0.2s">
													<!-- Post Thumbnail -->
													<div class="post-thumbnail">
														<img src="<?=base_url()?>assets/images/b2.jpg" alt="" style="width:100%;height:auto">
														<div class="post-meta">
																<p><a href="#" class="post-author">Katy Liu</a> on <a href="#" class="post-date">Sep 29, 2017 at 9:48 am</a></p>
															</div>
													</div>
													<!-- Post Content -->
													<div class="post-content">
														<a href="#" class="headline">
															<h5 style="font-weight:bold;font-color: orange;text-decoration: none;color: orange;font-size: 1.3vw;">Posko di Pelabuhan Anyer Butuh Bantuan</h5><br
														</a>
														<!-- Post Meta -->
														
													</div>
												</div>
											</div>
									  
								  </div>
								  </div>
				
						
				  </div>
				  <div class="col-md-4">
						<div class="world-latest-articles">
								<div class="row">
									<div class="title">
										<h5>Twitter BMKG</h5>
									</div>
								<div class="single-blog-post post-style-4 d-flex align-items-center wow fadeInUpBig" data-wow-delay="0.2s">
									<a class="twitter-timeline" class="twitter" style="margin-left:2%" data-width="auto" data-height="600" data-theme="dark" href="https://twitter.com/infoBMKG?ref_src=twsrc%5Etfw">Tweets by infoBMKG</a> <script async src="https	://platform.twitter.com/widgets.js" charset="utf-8"></script>
								</div>
								</div>
							</div>
							
						</div>



                        
			
				  </div>
				
			 
			</div>
		  </div>